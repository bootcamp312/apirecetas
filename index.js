const express = require('express');
const mongoose = require('mongoose');
const userSchema = require('./models/user');
const recipeSchema = require('./models/recipe');


const app = express()

app.use(express.json())

//mongoose.connect('mongodb://poli:password@localhost:27017/recipes?authSource=admin')
mongoose.connect('mongodb://poli:password@mongoRecetas:27017/recipes?authSource=admin')
.then(() => console.log('conectado a Mongo'))
.catch((error) => console.error(error))

app.get('/', (req, res) => {
    res.send('ok')
})

app.post('/new_user',(req,res) => {
    const user = userSchema(req.body);
    user.save()
    .then((data) => res.json(data))
    .catch((error) => res.send(error))
})

app.post('/new_recipe', (req,res) => {
    const recipe = recipeSchema(req.body);
    recipe.save()
    .then((data) => {
        console.log('Receta insertada')
        res.json(data)
    })
    .catch((error) => res.send(error))
})

app.post('/rate', (req,res) => {
    const { recipeId, userId, rating } = req.body
    recipeSchema.updateOne(
        {_id: recipeId},
        [
            { $set: { ratings: {$concatArrays: [{$ifNull: ['$ratings',[]]},[{ userId:userId, rating:rating}]]}}},
            {$set: {avgRating: {$trunc: [{$avg:['$ratings.rating']},0 ]}}}
        ]
    )
    .then((data) => {
        console.log('Rating procesado')
        res.json(data)
    })
    .catch( (error) => {
        console.log(error)
        res.send(error)
    });

})

app.get('/recipes', (req,res) => {
    const {userId,recipeId} = req.body
    if (recipeId){
        recipeSchema
            .find({_id : recipeId})
            .then((data) => {
                console.log('Se lista la receta solicitada')
                res.json(data)
            })
            .catch((error)=>res.json({message: error}))
    } else {
        recipeSchema
            .find({userId : userId})
            .then((data) => {
                console.log('Se listan todas las recetas del user')
                res.json(data)
            })
            .catch((error)=>res.json({message: error}))
    }
})

app.get('/recipesbyingredient', (req,res) => {
    const {ingredients} = req.body
    const ingredientsArray = ingredients.map(a=>a.name)
    console.log('ingredientsArray')
    console.log(ingredientsArray)
    recipeSchema
        .find({$expr:{$setIsSubset: ["$ingredients.name",ingredientsArray]}})
        .then((data) => {
            console.log('Se listan todas las recetas con los ingredientes solicitado')
            res.json(data)
        })
        .catch((error)=>res.json({message: error}))
})

app.get('/users_all', (req,res) => {
    const {} = req.body
    userSchema
        .find({})
        .then((data) => {
            console.log('Se listan todos los usuarios')
            res.json(data)
        })
        .catch((error)=>res.json({message: error}))

    // res.send("Enviamos las recetas")
})


app.listen(3000,() => console.log("Esuchando en el puerto 3000..."))